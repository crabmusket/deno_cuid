let getRandomValue;

if (crypto) {
  const lim = Math.pow(2, 32) - 1;
  getRandomValue = function() {
    return Math.abs(crypto.getRandomValues(new Uint32Array(1))[0] / lim);
  };
} else {
  getRandomValue = Math.random;
}

export default getRandomValue;
