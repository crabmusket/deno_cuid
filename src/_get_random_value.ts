const lim = Math.pow(2, 32) - 1;

export default function random(): number {
  return Math.abs(crypto.getRandomValues(new Uint32Array(1))[0] / lim);
}
