import { runTests, test } from "https://deno.land/std/testing/mod.ts";
import { assert, assertEquals } from "https://deno.land/std/testing/asserts.ts";
import { cuid, shortCuid, isCuid, isShortCuid, fingerprint } from "../mod.ts";
import defaultCuid from "../mod.ts";

test({
  name: "cuid()",
  fn() {
    assertEquals(typeof cuid(), "string");
  }
});

test({
  name: "default export",
  fn() {
    assert(defaultCuid === cuid);
  }
});

test({
  name: "isCuid()",
  fn() {
    let id = cuid();
    assert(isCuid(id) === true);
    assert(isCuid("abcdefghijklmnopqrstuvwxy") === false);
    assert(isCuid("") === false);
  }
});

test({
  name: "shortCuid()",
  fn() {
    assertEquals(typeof shortCuid(), "string");
  }
});

test({
  name: "isShortCuid()",
  fn() {
    let slug = shortCuid();
    assert(isShortCuid(slug) === true);
    assert(isShortCuid("") === false);
  }
});

test({
  name: "fingerprint",
  fn() {
    assertEquals(typeof fingerprint, "string");
    assert(fingerprint.length > 0);
  }
});

test({
  name: "cuid collisions",
  fn() {
    assert(collisionTest(cuid));
  }
});

test({
  name: "shortCuid collisions",
  fn() {
    assert(collisionTest(shortCuid));
  }
});

runTests();

const MAX = 1200000;

function collisionTest(fn) {
  var i = 0;
  var ids = {};
  var pass = true;

  while (i < MAX) {
    var id = fn();

    if (!ids[id]) {
      ids[id] = id;
    } else {
      pass = false;
      console.log("Failed at " + i + " iterations.");
      break;
    }

    i++;
  }

  return pass;
}
