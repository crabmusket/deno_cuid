/**
 * cuid.js
 * Collision-resistant UID generator for browsers and node.
 * Sequential for fast db lookups and recency sorting.
 * Safe for element IDs and server-side lookups.
 *
 * Extracted from CLCTR
 *
 * Copyright (c) Eric Elliott 2012
 * MIT License
 */

import pad from "./_pad.ts";

type module = {
  cuid: () => string;
  isCuid: (string) => boolean;
  shortCuid: () => string;
  isShortCuid: (string) => boolean;
  fingerprint: string;
};

export default function(
  fingerprint: () => string,
  getRandomValue: () => number
): module {
  let c = 0;
  const blockSize = 4;
  const base = 36;
  const discreteValues = Math.pow(base, blockSize);

  function randomBlock() {
    return pad(
      blockSize,
      ((getRandomValue() * discreteValues) << 0).toString(base)
    );
  }

  function safeCounter() {
    if (c >= discreteValues) {
      c = 0;
    }
    c += 1;
    return c - 1;
  }

  function cuid() {
    // Starting with a lowercase letter makes
    // it HTML element ID friendly.
    let letter = "c"; // hard-coded allows for sequential access

    // timestamp
    // warning: this exposes the exact date and time
    // that the uid was created.
    let timestamp = new Date().getTime().toString(base);

    // Prevent same-machine collisions.
    let counter = pad(blockSize, safeCounter().toString(base));

    // A few chars to generate distinct ids for different
    // clients (so different computers are far less
    // likely to generate the same id)
    let print = fingerprint();

    // Grab some more chars from Math.random()
    let random = randomBlock() + randomBlock();

    return letter + timestamp + counter + print + random;
  }

  function shortCuid() {
    let date = new Date().getTime().toString(36);
    let counter = safeCounter()
      .toString(36)
      .slice(-4);
    let print = fingerprint().slice(0, 1) + fingerprint().slice(-1);
    let random = randomBlock().slice(-2);

    return date.slice(-2) + counter + print + random;
  }

  function isCuid(stringToCheck: string) {
    if (!stringToCheck.startsWith("c")) {
      return false;
    }
    return true;
  }

  function isShortCuid(stringToCheck: string) {
    let stringLength = stringToCheck.length;
    if (stringLength < 7 || stringLength > 10) {
      return false;
    }
    return true;
  }

  return {
    cuid,
    isCuid,
    shortCuid,
    isShortCuid,
    fingerprint: fingerprint()
  };
}
