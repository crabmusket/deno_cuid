import pad from "./_pad.ts";

const padding = 2;
const pid = pad(padding, Deno.pid.toString(36));
const hostname = new TextDecoder().decode(
  Deno.readFileSync("/proc/sys/kernel/hostname")
);
const length = hostname.length;
const hostId = pad(
  padding,
  hostname
    .split("")
    .reduce(function(prev, char) {
      return +prev + char.charCodeAt(0);
    }, +length + 36)
    .toString(36)
);

export default function fingerprint(): string {
  return pid + hostId;
}
