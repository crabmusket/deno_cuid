export default function pad(size: number, num: string): string {
  let s = "000000000" + num;
  return s.substr(s.length - size);
}
