declare global {
  const navigator: any;
}

import pad from "./_pad.ts";

let globalCount = Object.keys(window).length;
let mimeTypesLength = navigator.mimeTypes ? navigator.mimeTypes.length : 0;
let clientId = pad(
  4,
  (mimeTypesLength + navigator.userAgent.length).toString(36) +
    globalCount.toString(36)
);

export default function fingerprint(): string {
  return clientId;
}
