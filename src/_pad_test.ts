import { runTests, test } from "https://deno.land/std/testing/mod.ts";
import { assert, assertEquals } from "https://deno.land/std/testing/asserts.ts";
import pad from "./_pad.ts";

test(function singleLetters() {
  assertEquals("00000001", pad(8, "1"));
  assertEquals("0000000a", pad(8, "a"));
  assertEquals("0001", pad(4, "1"));
  assertEquals("000_", pad(4, "_"));
});

test(function multipleLetters() {
  assertEquals("00000011", pad(8, "11"));
  assertEquals("0000abcd", pad(8, "abcd"));
});

test(function tooMuchPadding() {
  // Can't add more than 9 zeroes
  assertEquals("0000000001", pad(20, "1"));
  assertEquals("0hello", pad(20, "hello"));
});

test(function tooMuchString() {
  // If the input string is too long, we just take the last `n` characters
  assertEquals("23456789", pad(8, "123456789"));
  assertEquals("234", pad(3, "12345678901234"));
});

runTests();
