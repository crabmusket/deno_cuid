import fingerprinter from './src/_fingerprint.ts';
import getRandomValue from './src/_get_random_value.ts';
import makeImpl from './src/_cuid.ts';

const impl = makeImpl(fingerprinter, getRandomValue);

/** Create and return a new cuid. This updates the global counter. */
export function cuid(): string {
  return impl.cuid();
}

/**
 * Performs a few simple checks for the cuid-ness of a string. This is not a
 * definitive test, more like a very loose heuristic. For example, the string
 * 'constabulary' passes.
 */
export function isCuid(str: string): boolean {
  return impl.isCuid(str);
}

/** Generate a short cuid-like identifier. This updates the global counter. */
export function shortCuid(): string {
  return impl.shortCuid();
}

/** Checks for short-cuid-ness in much the same loose fashion as `isCuid`. */
export function isShortCuid(str: string): boolean {
  return impl.isShortCuid(str);
}

/**
 * The fingerprint should be somewhat unique for each 'instance' of this library.
 * The creation algorithm currently makes use of the process ID and hostname.
 */
export const fingerprint = impl.fingerprint;

export default cuid;
