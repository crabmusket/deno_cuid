# cuid

Collision-resistant ids optimized for horizontal scaling and binary search lookup performance.

This is a port of https://github.com/ericelliott/cuid for use with https://deno.land.
So far this port has only been tested on Linux and in the browser.
The server script will not work on Windows, and probably not on OSX; the browser script should work everywhere.

## Use on the server

`deno_cuid_example.ts`:
```ts
import { cuid, shortCuid } from "https://gitlab.com/crabmusket/deno_cuid/raw/v3.0.0/mod.ts";

console.log(cuid());
console.log(shortCuid());
```

Run the example script (output shown):
```bash
$ deno deno_cuid_example.ts
Download https://gitlab.com/crabmusket/deno_cuid/raw/v3.0.0/mod.ts
Download https://gitlab.com/crabmusket/deno_cuid/raw/v3.0.0/src/_fingerprint.ts
Download https://gitlab.com/crabmusket/deno_cuid/raw/v3.0.0/src/_get_random_value.ts
Download https://gitlab.com/crabmusket/deno_cuid/raw/v3.0.0/src/_cuid.ts
Download https://gitlab.com/crabmusket/deno_cuid/raw/v3.0.0/src/_pad.ts
⚠️  Deno requests read access to "/proc/sys/kernel/hostname". Grant? [a/y/n/d (a = allow always, y = allow once, n = deny once, d = deny always)] y
cjy181fgz0000r13p6fljjb0c
h01rphx
```

When runnning on the server, this library reads `/proc/sys/kernel/hostname` in order to create the [fingerprint](https://github.com/ericelliott/cuid#fingerprints) which forms part of the cuid.

## Use in-browser

Deno has built-in bundling which we can use to create a script to use in-browser.
Note that you must import `mod_browser.ts`.

`deno_cuid_browser_example.ts`:
```ts
import { cuid } from "https://gitlab.com/crabmusket/deno_cuid/raw/v3.0.0/mod_browser.ts";

declare global {
  const document: any;
}

export function main() {
  for (let i = 0; i < 10; i++) {
    document.write(cuid() + "<br />");
  }
}
```

Create the main app bundle by running:
```bash
deno bundle deno_cuid_browser_example.ts
```

`index.html`:
```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.6/require.min.js"></script>
<script src="deno_cuid_browser_example.bundle.js"></script>
<script>
  requirejs(["deno_cuid_browser_example"], pkg => pkg.main());
</script>
```

Run the local file server:
```bash
deno run --allow-net --allow-read https://deno.land/std/http/file_server.ts
```
Now open http://localhost:4500

## Why is this module on v3 already?

When I originally ported this repo, I gave it the same version as the existing JS library, which was 2.x.
This was probably a bad idea.
Anyway, when I ported the code to TypeScript and changed the exported API to be more deno-y, I bumped the major version number, so here we are at version 3.
